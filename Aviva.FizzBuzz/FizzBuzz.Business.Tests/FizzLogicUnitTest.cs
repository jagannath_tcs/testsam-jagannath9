﻿namespace FizzBuzz.Business.Tests
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using NUnit.Framework;
    using FluentAssertions;
    using Business;

    [TestClass]
    public class FizzLogicUnitTest
    {
        private FizzLogic fizzLogic;

        [SetUp]
        public void TestInitialize()
        {
            this.fizzLogic = new FizzLogic();
        }

        [TestCase(5, false)]
        [TestCase(9, true)]
        public void IsNumberDivisibleMethodReturnsValidFlagForGivenNumber(int inputFizzBuzzNumber, bool expectedResult)
        {
            var actualResult = this.fizzLogic.IsNumberDivisible(inputFizzBuzzNumber);

            actualResult.ShouldBeEquivalentTo(expectedResult);
        }

        [TestCase(false, "Fizz")]
        [TestCase(true, "Wizz")]
        public void GetDisplayMessageMethodReturnsFizzOrWizzForGivenInput(bool isTodayWednesday, string expectedResult)
        {
            var actualResult = this.fizzLogic.GetDisplayMessage(isTodayWednesday);

            actualResult.ShouldBeEquivalentTo(expectedResult);
        }
    }
}
