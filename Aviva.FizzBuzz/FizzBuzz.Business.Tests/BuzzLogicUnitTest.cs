﻿namespace FizzBuzz.Business.Tests
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using NUnit.Framework;
    using FluentAssertions;

    [TestClass]
    public class BuzzLogicUnitTest
    {
        private BuzzLogic buzzLogic;

        [SetUp]
        public void TestInitialize()
        {
            this.buzzLogic = new BuzzLogic();
        }

        [TestCase(10, true)]
        [TestCase(8, false)]
        public void IsNumberDivisibleMethodReturnsValidFlagForGivenNumber(int inputFizzBuzzNumber, bool expectedResult)
        {
            var actualResult = this.buzzLogic.IsNumberDivisible(inputFizzBuzzNumber);

            actualResult.ShouldBeEquivalentTo(expectedResult);
        }

        [TestCase(false, "Buzz")]
        [TestCase(true, "Wuzz")]
        public void GetDisplayMessageMethodReturnsBuzzOrWuzzForGivenInput(bool isTodayWednesday, string expectedResult)
        {
            var actualResult = this.buzzLogic.GetDisplayMessage(isTodayWednesday);

            actualResult.ShouldBeEquivalentTo(expectedResult);
        }

    }
}
